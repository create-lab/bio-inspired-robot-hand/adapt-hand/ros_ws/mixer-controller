import numpy as np
import json
from copy import deepcopy as cp
import os
import time

from helper_functions.tendon_regulator import TendonRegulator
from helper_functions.utility import Key

class MixerBinder:
    def __init__(self) -> None:

        self.tendonRegulator = TendonRegulator()        
        self.tendon_zero_dict = self.tendonRegulator.get_empty_tendon_array()
        self.tendon_limits = self.tendonRegulator.get_tendon_limits()
        self.mixer_input = np.zeros(12)

        self.key = Key()
        self.virtual_state_increment = 0.0002

        dir_path =  os.path.dirname(os.path.realpath(__file__))
        self.poses = json.load(open(dir_path + "/pose.config.json"))

    def get_virtual_mixer_input(self):
        increase_key = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=']
        decrease_key = ['!', '"', '£', '$', '%', '^', '&', '*', '(', ')', '_', '+']
        for i in range(12):
        
            if self.key.keyPress == increase_key[i]:
                self.mixer_input[i] += self.virtual_state_increment
                break

            elif self.key.keyPress == decrease_key[i]:
                self.mixer_input[i] -= self.virtual_state_increment
                break

            if self.mixer_input[i] < 0:
                self.mixer_input[i] = 0

            if self.mixer_input[i] > 1:
                self.mixer_input[i] = 1

    def get_mixer_input(self, raw_mixer_signal):
        for i in range(12):
            self.mixer_input[i] = raw_mixer_signal[i] / 4092

    def bind_tendons_to_mixer(self):

        tendon_positions = cp(self.tendon_zero_dict)

        for i, tendon_name in enumerate(tendon_positions.keys()):
            min_pos = self.tendon_limits[tendon_name]["min"]
            range = self.tendon_limits[tendon_name]["range"]

            tendon_positions[tendon_name] = self.mixer_input[i] * range + min_pos
            tendon_positions[tendon_name] = round(tendon_positions[tendon_name], 3)
        
        return tendon_positions
        

    def bind_pose_to_mixer(self):
    
        tendon_positions = cp(self.tendon_zero_dict)

        for i, key in enumerate(self.poses.keys()):

            tendon_pos_for_this_pose = self.poses[key]

            for tendon_name in tendon_positions.keys():
                tendon_positions[tendon_name] += tendon_pos_for_this_pose[tendon_name] * self.mixer_input[i]
                tendon_positions[tendon_name] = round(tendon_positions[tendon_name], 3)

        return tendon_positions