from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():

    mixer_controller_node = Node(
            package='mixer_controller',
            executable='mixer_controller',
            name='mixer_controller',
            output='screen',
            parameters=[
            {"virtual_mixer": True}
        ]
        )
    
    return LaunchDescription([
        mixer_controller_node
    ])