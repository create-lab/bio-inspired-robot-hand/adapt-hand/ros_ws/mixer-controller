from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():


    microros_agent_node =   Node(
            package="micro_ros_agent",
            executable="micro_ros_agent",
            arguments=["serial", "--dev", "/dev/ttyACM0"],
            output="screen",
        )
    
    mixer_controller_node = Node(
            package='mixer_controller',
            executable='mixer_controller',
            name='mixer_controller',
            output='screen',
            parameters=[
            {"virtual_mixer": False}
        ]
        )
    
    return LaunchDescription([
        # microros_agent_node,
        mixer_controller_node
    ])